#!/bin/bash

CONTAINER_NAME="nginx_lab"
IMAGE_NAME="jduttweiler/nginx:lab"
HTTP_PROXY_ARG="http_proxy=http://10.10.254.219:3128/"
HTTPS_PROXY_ARG="https_proxy=http://10.10.254.219:3128/"
NO_PROXY_ARG="no_proxy=localhost,::1,127.0.0.1,santafe.gob.ar,santafe.gov.ar,10.1.15.158"

build(){
    echo "[$(date +%Y-%m-%dT%H:%M:%S)] Building image..."
    docker build -t ${IMAGE_NAME} \
         --build-arg ${HTTP_PROXY_ARG} \
         --build-arg ${HTTPS_PROXY_ARG} \
         --build-arg ${NO_PROXY_ARG} \
         .
}

run(){
    echo "[$(date +%Y-%m-%dT%H:%M:%S)] Starting containers"
    docker run -d --rm \
            -e ${HTTP_PROXY_ARG} \
            -e ${HTTPS_PROXY_ARG} \
            -e ${NO_PROXY_ARG} \
            --name ${CONTAINER_NAME} \
            ${IMAGE_NAME}
}


#MAIN!
docker stop ${CONTAINER_NAME}
build
run

echo "END AT [$(date +%Y-%m-%dT%H:%M:%S)]"